(*
*	New features:
*	+ Write OCAML code inside chat. Inputting '/# yourCode' (without quotes), 
*	  each user will gets their own.ml file to work with. 
*	  Input '/$' to compile and run.
*	+ Share OCAML code inside chat. Using '/cu anotherUser', users
*	  can view each other's code.
*	+ Check code you have written, using '/cc'
*	+ Manually delete your own code, using '/del'
*
*	Build with: ocamlfind ocamlc -package str -package lwt -package lwt.unix -linkpkg -o server str.cma funSrv.ml funSrvMain.ml
*	No other libraries needed :)
*)
open Lwt



(*
*	+ sessions: 
*	A list associating user nicknames to the output channels that write
*	to their connections.
*	+ funSrvC:
*	A list associating user nicknames to the code that they have written.
*)
let sessions = ref []
let funSrvC = ref []
exception Quit



(*
*	Updates code association when user provides new lines.
*	Locks the list while each user's code is being updated.
*
*	temp reference backs up code already written by user. The user is then
*	removed from the association, then readded with the new line of code
*	appended with backed up code.
*)
let update_code nn mut l = 
	let _ = Lwt_mutex.lock mut in ();
	let temp = ref "" in Lwt.return ()
	>>= fun () -> 
		if (List.mem_assoc nn !funSrvC) then
			(
			let _ = 
			temp := (List.assoc nn (!funSrvC)); in
			Lwt.return ()
			>>= fun () -> funSrvC := List.remove_assoc nn (!funSrvC);
			Lwt.return ()
			>>= fun () -> funSrvC := (nn, (!temp^"\n"^l))::(!funSrvC); 
			Lwt.return ()
			>>= fun () -> Lwt_mutex.unlock mut; Lwt.return ()
			)
		else
			(
			funSrvC := (nn, ("\n"^l))::(!funSrvC); Lwt.return ()
			>>= fun () -> Lwt_mutex.unlock mut; Lwt.return ()
			)
			
			
let delete_code nn mut = 
	Lwt_mutex.lock mut
	>>= fun () -> funSrvC := List.remove_assoc nn (!funSrvC);
	Lwt.return ()
	>>= fun () -> Lwt_mutex.unlock mut; Lwt.return ()


(*
*	+ Broadcasts to every user in sessions list except sender themselves.
*)
let rec broadcast sender msg = Lwt_list.iter_p (fun x ->
	( match x with
	| (nm, outChannel) -> 
		if (nm = sender) then
			Lwt.return ()
		else
			Lwt_io.fprintl outChannel (sender^": "^msg) 
	)
	) !sessions



(*
*	+ Broadcasts to current user only.
*)
let rec userMsg outp msg = 
	Lwt_io.fprintl outp msg
	
	
	
(*
*	+ Remove a session from the list of sessions.
*	+ Remove coder from list of users and code, funSrvC.
*	+ Remove a user's ocaml files once the user has left
*	+ Broadcast that the person has left the chat.
*)
let remove_session nn =
 	funSrvC := List.remove_assoc nn !funSrvC;
  	sessions := List.remove_assoc nn !sessions;
	let _ = Sys.command ("rm " ^ (nn)^"funSrvCode.ml") in ();
	let _ = Sys.command ("rm " ^ (nn)^"funSrvCode.cmi") in ();
	let _ = Sys.command ("rm " ^ (nn)^"funSrvCode.cmo") in ();
	let _ = Sys.command ("rm " ^ (nn)^"funSrvOut.txt") in ();
	let _ = Sys.command ("rm " ^ (nn)^"funSrvCode") in ();
	let _ = Lwt.return () in ();
  	broadcast nn "<left chat>" >>= fun () ->
  	Lwt.return ()



(*
*	+ Closes both input and output channels.
*	+ Calls remove_session to remove user from sessions list. 
*)
let handle_error e nn inp outp = 
	Lwt_io.close inp
	>>= fun () -> Lwt_io.close outp
	>>= fun () -> remove_session nn



(*
*	+ List all users in sessions list ONLY for user that requested.
*)
let list_users outp =
	Lwt_list.iter_p (fun x ->
	( match x with
	| (nm, outChannel) -> 
		if (nm = "") then
			Lwt.return ()
		else
			Lwt_io.fprintl outp ("user " ^ nm)
	) ) (!sessions)
	
	
	
(*	
*	Changes nickname.
*	+ Locks thread,
*	+ Removes old nickname association from sessions list,
*	+ Adds new nickname assocation to sessions list,
*	+ Announces to other users that old nickname has changed to new nickname.
*)
let change_nn nn outp new_nn mut = 
	Lwt_mutex.lock mut 
	>>=	fun () -> sessions := List.remove_assoc nn (!sessions); 
	Lwt.return ()
	>>= fun () -> sessions := (new_nn, outp)::(!sessions); 
	broadcast nn ("has changed their named to "^new_nn)
	>>= fun() -> Lwt_mutex.unlock mut; 
	Lwt.return ()
	


(*
*	+ Obtain initial nickname.
*	+ Add (nickname, outp) to !sessions.
*	  Write is mutex locked so that sessions is not modified by two threads,
*	  then released when complete.
*	+ Announce join to other users.
*)
let rec login_handler nr (inp,outp) = 
	Lwt_io.fprintl outp "Enter initial nick:"
	>>= fun () -> Lwt_io.read_line inp
	>>= fun x -> 
		if (x = "") then
			let _ = Lwt_io.fprintl outp "Can't be a ghost!" in
			login_handler nr (inp, outp)
		else
			let _ = nr := x in Lwt.return (); 
	>>= fun () -> let m = Lwt_mutex.create () in Lwt_mutex.lock m
	>>= fun () -> let _ = sessions := (!nr, outp)::(!sessions) in Lwt.return ()
	>>= fun () -> let _ = Lwt_mutex.unlock m in Lwt.return ()
	>>= fun () -> let _ = broadcast !nr "<joined>" in Lwt.return ()
	
	
	
(*
*	+ Compile and run a user's code.
*	Creates user's personal .ml file using unix terminal command "touch <file>",
*	opens ocaml output stream to specified file,
*	writes associated code of user that requested compilation,
*	then clear the stream.
*
*	Send compile command to unix terminal and run.
*	Redirect results to an output file.
*
*	Open ocaml input stream to specified output file,
*	Keep accepting line input until an End_of_file is reached.
*	Finally, send result of program to user.
*)
let command_system nr outp = 
	let _ = Sys.command ("touch "^(nr)^"funSrvCode.ml") in ();
	let oc = open_out ((nr)^"funSrvCode.ml") in
	output_string oc (List.assoc nr (!funSrvC));
	close_out oc;
	
	let _ = Sys.command ("ocamlc -o "
	^(nr)^"funSrvCode "
	^(nr)^"funSrvCode.ml") in ();
	
	let _ = Sys.command ("./"
	^(nr)^"funSrvCode >> "
	^(nr)^"funSrvOut.txt") in ();
	
	let ic = open_in ((nr)^"funSrvOut.txt") in
	let myFunSrvCout = ref "" in
	try (
		while true do
			myFunSrvCout := (!myFunSrvCout)^(input_line ic);
			done
	) with End_of_file -> ();
	let _ = userMsg outp (!myFunSrvCout) in ()



(*
*	+ Detects action commands.
*	+ Raises Quit exception to quit, which will make handle_error and
*	  remove_session clean up channels and user.
*	+ New nickname will call change_nn which handles sessions list, 
*	  nick reference is also reassigned.
*	+ Users list calls list_users function.
*	+ /# and /$ updates code in association list. /$ will compile and run
*	  user's file via server.
*	+ /cc to see current user's code. Checks if empty.
*	+ /del to delete current user's code. Checks if user has written any.
*	+ /cu to see another user's code. Checks if empty and valid user.
*	+ /help for instructions on added features
*)
let rec handle_input nr outp l = 
	if ((String.length l >= 2) && (Str.string_before l 2 = "/q")) then
		Lwt.fail Quit
		
	else if ((String.length l >= 2) && (Str.string_before l 2 = "/n")) then
		if ((String.length l = 3) || (String.length l =2) 
		&& (Str.string_before l 2 = "/n")) then
			Lwt_io.fprintl outp "Please provide a new nickname:"
		else if ((String.length l = 4) && (Str.string_after l 3 = " ")) then
			Lwt_io.fprintl outp "Can't be a ghost!"
		else
			let _ = change_nn !nr outp (Str.string_after l 3) (Lwt_mutex.create ()) in
			nr := (Str.string_after l 3); Lwt.return ()
		
	else if ((String.length l >= 2) && (Str.string_before l 2 = "/l")) then
		list_users outp
		
	else if ((String.length l >= 2) && (Str.string_before l 2 = "/#")) then
		let _ = update_code !nr (Lwt_mutex.create ()) (Str.string_after l 2)
		in Lwt.return ();
		
	else if ((String.length l >= 2) && (Str.string_before l 2 = "/$")) then
		let _ = update_code !nr (Lwt_mutex.create ()) (Str.string_after l 2)
		in ();
		let _ = command_system (!nr) outp in Lwt.return ()
		
	else if ((String.length l >= 3) && (Str.string_before l 3 = "/cc")) then
		if (List.mem_assoc (!nr) (!funSrvC)) then
			userMsg outp (List.assoc (!nr) (!funSrvC))
		else
			userMsg outp ("You have not written any code yet. Type /help for info.")
	
	else if ((String.length l >= 4) && (Str.string_before l 4 = "/del")) then
		if (List.mem_assoc (!nr) (!funSrvC)) then
			let _ = delete_code !nr (Lwt_mutex.create ()) in Lwt.return ()
		else
			userMsg outp ("You have not written any code to delete yet. Type /help for info.")
			
	else if ((String.length l >= 3) && (Str.string_before l 3 = "/cu")) then
		if ((String.length l = 3) && (Str.string_before l 3 = "/cu")) then
			userMsg outp "Please provide a current username!"
		else if (List.mem_assoc (Str.string_after l 4) (!funSrvC)) then
			userMsg outp (List.assoc (Str.string_after l 4) (!funSrvC))
		else
			userMsg outp "That user is not valid, or they have not written any code yet!\n Type /help for info."
			
	else if ((String.length l >= 5) && (Str.string_before l 5 = "/help")) then
		userMsg outp "************************\n
		To write code to the server, type: /# yourCodeHere\n
		Example:\n
		/# let () = print_endline \"Make America Great Again! \"
		/# let () = print_endline \"Make Canada Great Again Eh? \"
		/# let () = print_endline \"Shi Zhongguo zaici weida! \"
		/# let () = print_endline \"Bikin Indonesia hebat lagi! \"
		/# let () = print_endline \"Confirm make Singapore great again or not leh? \"\n
		All five lines will be saved in your own OCAML source file instance.
		Only you may edit your own code.\n\n
		To compile and run, type: /$
		To check what you have written, type: /cc
		To clear your code, type: /del
		To check someone else's code in the chat, type: /cu theirUserName
		\n************************"
	else
		broadcast !nr l



let chat_handler (inp,outp) =
  let nick = ref "" in
  let _ = login_handler nick (inp,outp) in
  let rec main_loop () =
	  Lwt_io.read_line inp >>= handle_input nick outp >>= main_loop in
  Lwt.async (fun () -> Lwt.catch main_loop (fun e -> handle_error e !nick inp outp))
