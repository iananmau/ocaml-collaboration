# OCaml Collaboration #

### What is this repository for? ###

OCaml chat client with code sharing and compiling driven by the host.

### Installing ###

*OCaml 4.01+

Lwt concurrency manual: http://ocsigen.org/lwt/manual/

Load module: 'module load soft/ocaml/4.01'

Compile: 'ocamlfind ocamlc -package str -package lwt -package lwt.unix -linkpkg -o server str.cma funSrv.ml funSrvMain.ml'

Run: './server (optional-port-number)'

Connect: telnet (IP)